#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <iomanip>
#include "TH2D.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TPaveText.h"
#include "TPolyLine.h"
#include "TF1.h"
#include "2DTools.h"
#include "TColor.h"
#include "TROOT.h"
#include "AtlasLabels.h"
#include "AtlasUtils.h"
#include "AtlasStyle.h"
#ifndef __CINT__
#include "AtlasLabels.C"
#include "AtlasUtils.C"
#include "AtlasStyle.C"
#endif

using namespace std;

struct contLabels{
    float x;
    float y;
    float angle;
    string label;
    int eo;
};

vector<contLabels> g_contLabels;

double getExclusionMass(TGraph *g,double w,double z, double h,bool isHtX){
    // Loop over points and find  the nearest to the excluded mass.
    // Asume a line between the two points.
    double mindiff = 999;
    double d = 50;
    int N = g->GetN();
    double *xi = g->GetX();
    double *yi = g->GetY();
    int init = -1;
    // If the lower mass is not excluded return 0
    // Find the initial points
    for (int i = 0; i < N-1; i++) {
        if(yi[i] <=  0.05 && yi[i+1] >= 0.05) {init = i;break;}
    }
    if (init == -1){
        if(yi[N-1] <= 0.05) return xi[N-1];
        //if(yi[0] >= 0.05) return xi[0];
        if(isHtX){init = 0;
            TCanvas *c = new TCanvas("tmp","tmp",800,600);
            g->SetMarkerSize(0.5);
            TF1 *f = new TF1("tofit","1/(1+exp(-[0]*(x-[1])))",100,1000);
            //TF1 *f = new TF1("tofit","[0]*exp([1]*x)",300,1000);
            f->SetParameter(0,0.01);
            f->SetParameter(1,600);
            g->Fit("tofit","QR");
            g->Draw("axl");
            ostringstream s;
            s<<"graph_"<<w<<"_"<<z<<"_"<<h<<".pdf";
            c->Print(s.str().c_str());
            double result = f->GetX(0.05);
            delete f;
            delete c;
            return result;
        }
        return 0;
    }

    double xb = xi[init], xt = xi[init+1]; 
    double yb = yi[init], yt = yi[init+1];
    while(mindiff > 1e-6){
        double m = (yt-yb)/(xt-xb);
        double a = yt - m*xt;
        double ym = a + m*(xb+d/2.);
        /*cout<<"p1 = ("<<xb<<","<<yb<<"), p2 = ("<<xt<<","<<yt<<")"<<endl;
          cout<<"pmed = ("<<xb+d/2<<","<<ym<<")"<<endl;*/
        if (ym >= 0.05) {
            yt = ym;
            xt = xb+d/2.;
        } else {
            yb = ym;
            xb = xb+d/2.;
        }
        d = d/2.;
        mindiff = fabs(yb - 0.05);
        /*cout<<"Mindiff: "<<mindiff<<endl;*/
    }
    return xb;
}

void printTable(TH2D *histo,string quark, string filename){

    ofstream file(filename.c_str());
    file.precision(2);
    file<<fixed<<showpoint;
    file<<setw(11)<<"|";
    for (int i = 1; i <= histo->GetNbinsX(); i++)
        file<<" WBr "<<0.05*(i-1)<<" |";
    file<<endl;
    for (int i = 1; i <= histo->GetNbinsY(); i++) {
        double h = 0.05*(i-1);
        file<<" HBr "<<h<<" |";
        for (int j = 1; j <= histo->GetNbinsX(); j++) {
            file<<setw(9)<<histo->GetBinContent(j,i)<<" |";
        }
        file<<endl;
    }    
    file.close();
}

// Based on the function I used in the 2D plots to know if it is in the edge of the region or not.
bool isEdge(TH2D *h, int x, int y, double value){
    // To be at the edge the value of the bin needs to be 0.05 and has neighbors which have value 1

    // Check if the point is at the edge of the histogram.
    int toLeft = (x == 1) ? 0 : 1;
    int toRight = (x == 21) ? 0 : 1;
    int toTop = (y == 21) ? 0 : 1;
    int toBottom = (y == 1) ? 0 : 1;

    //cout<<"Checking edge for mass "<<value<<endl;
    // Check it svalue and compare with neighbors
    //cout<<"Bincontent("<<x<<","<<y<<"): "<<h->GetBinContent(x,y)<<endl;
    if (h->GetBinContent(x,y) >= value){
        /*cout<<"Bincontent("<<x<<","<<y<<"): "<<h->GetBinContent(x,y)<<endl;
          cout<<"Left("<<x-toLeft<<","<<y<<"): "<<h->GetBinContent(x-toLeft,y)<<endl;
          cout<<"Right("<<x+toRight<<","<<y<<"): "<<h->GetBinContent(x+toRight,y)<<endl;
          cout<<"Bottom("<<x<<","<<y-toBottom<<"): "<< h->GetBinContent(x,y-toBottom)<<endl;
          cout<<"Top("<<x<<","<<y+toTop<<"): "<< h->GetBinContent(x,y+toTop)<<endl;*/
        if ((x+y) == 22) return true;

        // Avid isolated points. Does not make too much sense to have a big not excluded region and in the middle
        // one exlusion point. It won't have any effect when the full analysis is done but in the meanwhile there are some
        // details that might lead to this.
        if (h->GetBinContent(x-toLeft,y) < value &&
                h->GetBinContent(x+toRight,y) < value &&
                h->GetBinContent(x,y-toBottom) < value &&
                h->GetBinContent(x,y+toTop) < value){
            //h->GetBinContent(x+toRight,y+toTop) > value &&
            //h->GetBinContent(x+toRight,y-toBottom) > value &&
            //h->GetBinContent(x-toLeft,y+toTop) > value &&
            //h->GetBinContent(x-toLeft,y-toBottom) > value)
            return false;
        }

        if (h->GetBinContent(x-toLeft,y) < value ||
                h->GetBinContent(x+toRight,y) < value ||
                h->GetBinContent(x,y-toBottom) < value ||
                h->GetBinContent(x,y+toTop) < value) return true;

        // If it is in the edge of the forbiden region there will be  no neighbors.
    }
    else{
        if ((x+y) == 22) return false;
        if (h->GetBinContent(x-toLeft,y) >= value &&
                h->GetBinContent(x+toRight,y) >= value &&
                h->GetBinContent(x,y-toBottom) >= value &&
                h->GetBinContent(x,y+toTop) >= value){
            //h->GetBinContent(x+toRight,y+toTop) < value &&
            //h->GetBinContent(x+toRight,y-toBottom) < value &&
            //h->GetBinContent(x-toLeft,y+toTop) < value &&
            //h->GetBinContent(x-toLeft,y-toBottom) < value)
            return true;
        }
        // If the point is not excluded but it is in the middle of a excluded region then it should be excluded
        // This part avoid small fluctuations
    }


    // In any other case return false
    return false;
}

void getContour(TH2D *h, vector<double> &x, vector<double> &y, double mass){

    float xorigin = 0;
    float yorigin = 0.0001;
    // This is will be used when different analysis are supported
    /*if (region == "Ztag") {
      xorigin = 0;
      yorigin = 0;
      }
      if (region == "Htag"){
      xorigin = 0;
      yorigin = 1;
      }
      if (region == "Wtag"){
      xorigin = 1;
      yorigin = 0;
      }*/

    double beforelastx, beforelasty;
    x.push_back(xorigin);
    y.push_back(yorigin);
    // Search for points that stand at the edge of the excluded region.
    // Loop over al points of the plot.
    bool stored;
    for (int nx = 0; nx < 21; nx++){
        float xpoint = nx*0.05;
        stored = false;
        int nstored = 0;
        vector<double> tmpy;
        for (int ny = 0; ny < 21-nx; ny++) {
            float ypoint = ny*0.05;
            if (ny == 0) ypoint = 0.0001;
            if (isEdge(h,nx+1,ny+1,mass)){
                cout<<"("<<xpoint<<","<<ypoint<<") is edge"<<endl;
                if (ny == 0){
                    beforelastx = xpoint;
                }
                // This pop_back is done to avoid vertical stright lines since the inner loopo is 
                // from bottom to top
                /*if (stored && xpoint != xorigin){
                  x.pop_back();
                  tmpy.pop_back();
                  nstored--;
                  }*/
                x.push_back(xpoint);
                tmpy.push_back(ypoint);
                stored = true;
                nstored++;
                continue;
            }
            // say it is not stored. This will clean pikes
            stored = false;
        }
        if (nstored > 1){
            reverse(tmpy.begin(),tmpy.end());
            /*for(int i = 1; i <= nstored; i++){
              y[y.size()-i] = tmpv[y.size()-nstored+i-1];
              }*/
        }
        for (int i = 0; i < tmpy.size(); i++) {
            y.push_back(tmpy[i]);
        }
        if(x[x.size()-1] == 0){
            vector<double>::iterator it = x.end();
            x.insert(it-nstored,-0.025);
            it = y.end();
            y.insert(it-nstored,*(it-nstored));
            /*x.push_back(-0.025);
              tmpy.push_back(tmpy[tmpy.size()-1]);*/
        }
    }
    //cout<<"Last point: ("<<beforelastx<<",0)"<<endl;
    x.push_back(beforelastx);
    y.push_back(-0.05);
    x.push_back(xorigin);
    y.push_back(yorigin);

}

void fillContLabels(string f,vector<contLabels> &v){
    if(f == "") return;
    ifstream file(f.c_str());
    string line;
    while(getline(file,line)){
        contLabels lab;
        stringstream read;
        read<<line;
        read >> lab.eo >> lab.x >> lab.y >> lab.angle >> lab.label;
        v.push_back(lab);
    }
    file.close();
}

void drawLabels(int eo,string file){
    vector<contLabels> labels;
    fillContLabels(file,labels);
    for(int i = 0; i < labels.size(); i++){
        if(labels[i].eo != eo) continue;
        TLatex tex;
        //tex.SetNDC();
        tex.SetTextSize(0.04);
        tex.SetTextColor(kWhite);
        tex.SetTextAngle(labels[i].angle);
        tex.DrawLatex(labels[i].x,
                labels[i].y,
                labels[i].label.c_str());
    }
}


/************************************************************
  Each part of the code can be modified depending on what the
  user needs. The code below is just an example with some
  explanations on how to use different parts of the script.
 *************************************************************/    
int TempPlot(string inputfiles, string quark, string initmarkers, string channel,string labels=""){
#ifdef __CINT__
    gROOT->LoadMacro("AtlasLabels.C");
    gROOT->LoadMacro("AtlasUtils.C");
    gROOT->LoadMacro("AtlasStyle.C");
#endif

    // Define ATLAS style. 
    SetAtlasStyle();

    // Set verbose in the readers
    bool verbose = false;


    // Create the output parser
    vector<OutputReader> readers;
    vector<string> inits = splitString(initmarkers);
    vector<string> names = splitString(inputfiles);
    // List of values to read
    // This might be modified by th user if the lines to read are different.
    // If several files are used and they don't have the same lines a different
    // vector can be created and used for that reader objetc with the setLinesToFind 
    // function. See below for the crate readers loop.
    vector<string> lines, linesHtWbComb;
    lines.push_back("gclsexpbm2");
    lines.push_back("gclsexpbm1");
    lines.push_back("gclsexpbmed");
    lines.push_back("gclsexpbp1");
    lines.push_back("gclsexpbp2");
    lines.push_back("1-CLb median  (sig)");
    lines.push_back("Observed CLs");
    lines.push_back("Observed CLb");

    // key values for the lines using Roostat
    linesHtWbComb.push_back("+2sigma");
    linesHtWbComb.push_back("+1sigma");
    linesHtWbComb.push_back("-1sigma");
    linesHtWbComb.push_back("-2sigma");
    linesHtWbComb.push_back("Median");
    linesHtWbComb.push_back("Observed");

    //Create readers
    for(int i = 0; i < names.size(); i++){
        cout<<"Reading file "<<names[i]<<endl;
        cout<<"Using init marker "<<inits[i]<<endl;
        OutputReader reader(names[i],quark);
        reader.setVerbose(verbose);
        reader.setInitMarker(inits[i]);
        reader.setLinesToFind(lines);//<- For each reader created with a given 
        // input file, use a different set of lines if needed
        if(names[i].find("roostat") != string::npos){ 
            reader.setLinesToFind(linesHtWbComb);
            reader.setPreffix("lim_unblind_Job_"); // <==== This is an example of what can be done if some text is added before the mass value 
            //       in the forlder name. 
        }
        else reader.setLinesToFind(lines);
        // Parser file
        reader.parse();
        readers.push_back(reader);
    }
    cout<<"Done parsing files"<<endl;



    // Masses
    const int nmass = 14;
    int masses[nmass] = {350,400,450,500,550,600,650,700,750,800,850,900,950,1000};

    // 2D histograms
    double bins[22];
    bins[0]=0;
    bins[1] = 0.025;
    bins[20] = 0.975;
    bins[21] = 1;
    for(int nb = 2; nb < 20; nb++) bins[nb] = bins[nb-1] + 0.05;
    double binsx[22];
    memcpy(&binsx[0],&bins[0],22*sizeof(double));
    binsx[0] = 0.0001;
    for(int i = 0; i < 22; i++) cout<<"Nbinsx: "<<binsx[i]<<endl;
    TH2D *exp2D = new TH2D("exp_h","exp_h",21,bins,21,binsx);
    TH2D *obs2D = new TH2D("obs_h","obs_h",21,bins,21,binsx);

    // Loop over branching ratios
    double expmin = 999999999;
    double obsmin = 999999999;
    int npoints = 21;
    for (int bw = 0; bw < npoints; bw++) {
        double brW = bw*0.05;
        for (int bh = 0; bh < npoints; bh++) {
            bool debug = false;//just to print some tests
            double brH = bh*0.05;
            if (brW+brH > 1) continue;
            double brZ = 1 - (brW + brH);
            if (brZ < 0.005) brZ = 0;
            double expExclusion = -9999;
            double obsExclusion = -9999;
            string keyToSearch_exp;
            string keyToSearch_obs;
            if (verbose) cout<<"BR: ("<<brW<<","<<brH<<","<<brZ<<")"<<endl;
            // Loop over all masses and inputs
            for (int r = 0; r < readers.size(); r++) {
                /// This is what will be searched for in the output file.
                // It can change depending on what the user wants.
                OutputReader reader = readers[r];
                bool roostat = reader.getFileName().find("roostat") != string::npos;
                //Here we choose what is the interesting value we want to read from the txt lines.
                //For the Roostat case it is the value named Median.
                keyToSearch_exp = (roostat) ? "Median" : "gclsexpbmed";
                keyToSearch_obs = (roostat) ? "Observed" : "Observed CLs";
                vector<double> xMassobs,xMassex;
                vector<double> yCls_exp, yCls_obs;
                for (int mi = 0; mi < nmass; mi++) {
                    double expcls = reader.getValue(brW,brH,brZ,masses[mi],keyToSearch_exp);
                    double obscls = reader.getValue(brW,brH,brZ,masses[mi],keyToSearch_obs);
                    if(expcls == -999){
                        cerr<<"WARNING: cls is -999 which means that "<<keyToSearch_exp<<" has not been found"<<endl;
                        //return 1;
                    }else{
                        yCls_exp.push_back(expcls);
                        xMassex.push_back((double)masses[mi]);
                    }

                    if(obscls == -999){
                        cerr<<"WARNING: cls is -999 which means that "<<keyToSearch_obs<<" has not been found"<<endl;
                        //return 1;
                    }else{
                        yCls_obs.push_back(obscls);
                        xMassobs.push_back((double)masses[mi]);
                    }
                }

                // Create graph of mass vs cls and fit them with an exponential function
                TGraph *g_exp = new TGraph(xMassex.size(),&xMassex[0], &yCls_exp[0]);
                TGraph *g_obs = new TGraph(xMassobs.size(),&xMassobs[0], &yCls_obs[0]);

                // Fits seem to be hard to do fine since the dependency is not clear and 
                // it depends in which region we are. Since for all BR there is at least one mass which is excluded
                // lets do it with a simple interpolation.
                // Find values for the excluded mass
                double tmpex = getExclusionMass(g_exp,brW,brZ,brH,reader.getFileName().find("combination")!=string::npos);
                double tmpob  = getExclusionMass(g_obs,brW,brZ,brH,reader.getFileName().find("combination")!=string::npos);
                expExclusion = (expExclusion < tmpex) ? tmpex : expExclusion;
                obsExclusion = (obsExclusion < tmpob) ? tmpob : obsExclusion;
                cout<<"Expected: "<<expExclusion<<endl;
                bool outofbound = false;
                if(expExclusion > 1000 || expExclusion < 0 ||
                        obsExclusion > 1000 || obsExclusion < 0) {
                    outofbound = true;
                    cout<<"Out of bound: exp "<<expExclusion<<endl;
                    cout<<"Out of bound: obs "<<obsExclusion<<endl;
                }
                delete g_exp;
                delete g_obs;
            }

            // Fill histograms
            /*brW = (brW == 1) ? 0.999 : brW;
              brH = (brH == 1) ? 0.999 : brH;
              brW = (brW == 0) ? 0.0001 : brW;
              brH = (brH == 0) ? 0.0001 : brH;*/
            if(obsmin > obsExclusion) obsmin = obsExclusion;
            if(expmin > expExclusion) expmin = expExclusion;
            exp2D->SetBinContent(bw+1,bh+1,expExclusion);
            obs2D->SetBinContent(bw+1,bh+1,obsExclusion);
        }
    }

    //  Artificially fill non physical region to avoid peaks in contours
    for(int i = 1; i < 21; i++){
        int j = 22-i;
        double value = exp2D->GetBinContent(i,j);
        exp2D->SetBinContent(i+1,j,value);
        value = obs2D->GetBinContent(i,j);
        obs2D->SetBinContent(i+1,j,value);
    }
    exp2D->SetStats(0);
    exp2D->SetTitle("");
    obs2D->SetStats(0);
    obs2D->SetTitle("");
    string xtitle, ytitle,ztitle;
    if(quark == "T"){
        xtitle = "BR (T #rightarrow Wb)";
        ytitle = "BR (T #rightarrow Ht)";
    }
    else {
        xtitle = "BR (B #rightarrow Wt)";
        ytitle = "BR (B #rightarrow Hb)";
    }

    exp2D->SetYTitle(ytitle.c_str());
    exp2D->SetXTitle(xtitle.c_str());
    exp2D->GetXaxis()->SetTitleSize(0.05);
    exp2D->GetYaxis()->SetTitleSize(0.05);
    exp2D->GetZaxis()->SetTitleOffset(1.4);
    obs2D->SetYTitle(ytitle.c_str());
    obs2D->SetXTitle(xtitle.c_str());
    obs2D->GetZaxis()->SetTitleOffset(1.4);
    obs2D->GetXaxis()->SetTitleSize(0.05);
    obs2D->GetYaxis()->SetTitleSize(0.05);

    exp2D->SetMaximum(950);
    exp2D->SetMinimum(500);
    obs2D->SetMaximum(950);
    obs2D->SetMinimum(500);

    /*exp2D->SetMinimum(expmin);
      obs2D->SetMinimum(obsmin);*/

    // Print tables with values
    printTable(exp2D,quark,"expectedValues_"+quark+".txt");
    printTable(obs2D,quark,"observedValues_"+quark+".txt");

    // Make the plot
    for (int exob = 0; exob < 2; exob++) {
        TCanvas *c= new TCanvas("c","c",1600,1200);

        ztitle = (exob == 0) ? "Expected 95\% CL mass limit [GeV]" : "Observed 95\% CL mass limit [GeV]";
        exp2D->SetZTitle(ztitle.c_str());
        obs2D->SetZTitle(ztitle.c_str());
        c->SetRightMargin(0.2);
        // Change default pallete
        const int number = 3;
        TColor *red4 = gROOT->GetColor(kRed);
        TColor *red = gROOT->GetColor(kRed-3);
        TColor *azure3 = gROOT->GetColor(kAzure-5);
        TColor *azurem5 = gROOT->GetColor(kAzure+6);
        double Red[number] = {azurem5->GetRed(),azure3->GetRed(),red4->GetRed()};
        double Green[number] = {azurem5->GetGreen(),azure3->GetGreen(),red4->GetGreen()};
        double Blue[number] = {azurem5->GetBlue(),azure3->GetBlue(),red4->GetBlue()};
        double length[number] = {0,0.5,1};
        TColor::CreateGradientColorTable(number,length,Red,Green,Blue,50);
        gStyle->SetNumberContours(50);
        
        // CONTOURS
        // Drawing contours in the temperature plot
        int contour[14] = {300,350,400,450,500,550,600,650,700,750,800,850,900,950}; // Masses used for the contours
        if(exob == 0){
            exp2D->Draw("colz ");
            for (int ii = 0; ii < 14; ii++){
                if(quark == "B" && ii < 8) continue; // These are used to avoid drawing contours 
                if(quark == "T" && ii < 5) continue; // that won't be visible or cannot have lables due to little space
                int mass = contour[ii];
                bool fiftys = ii%2 != 0;
                TH2D *clon = (TH2D*)exp2D->Clone("expclon");
                clon->SetContour(1);
                clon->SetLineWidth(2.5);
                clon->SetContourLevel(0,mass);
                clon->SetLineStyle(0.5);
                clon->SetLineColor(kWhite);
                clon->SetLineStyle(1);
                if (fiftys) clon->SetLineStyle( 7 );
                clon->Draw("cont3 same");
            }
        }
        else{
            obs2D->Draw("colz ");
            for (int ii = 0; ii < 14; ii++){
                if(quark == "B" && ii < 8) continue; // These are used to avoid drawing contours 
                if(quark == "T" && ii < 5) continue; // that won't be visible or cannot have lables due to little space 
                int mass = contour[ii];
                bool fiftys = ii%2 != 0;
                TH2D *clon = (TH2D*)obs2D->Clone("expclon");
                clon->SetContour(1);
                clon->SetContourLevel(0,mass);
                clon->SetLineWidth(2.5);
                clon->SetLineColor(kWhite);
                clon->SetLineStyle(1);
                if (fiftys) clon->SetLineStyle( 7 );
                clon->Draw("cont3 same");
            }
        }

        // Draw forbiden region
        float x_forb[4] = {0, 1, 1, 0};
        float y_forb[4] = {1, 1, 0, 1};
        TPolyLine *pForb = new TPolyLine(4,x_forb,y_forb);
        pForb->SetFillColor(kWhite);
        //pForb->SetFillStyle(3004);
        pForb->SetLineColor(kBlack);
        pForb->Draw("f");
        pForb->Draw();

        // Write text
        string energy = "#sqrt{s} = 8 TeV";
        string lumi = "#sqrt{s} = 8TeV, 20.3 fb^{-1}";
        //string lumi2 = "L = 14.3 fb^{-1}";
        string vlquark = "Zb/t + X";
        if(channel == "Combination"){
            ATLASLabel(0.275,0.87,"Preliminary");
            myText(0.275,0.825,1,lumi.c_str(),0.042);
            myText(0.65,0.75,1,vlquark.c_str());
            myText(0.58,0.6,1,channel.c_str());
            myText(0.555,0.67,1,"Dilep. + Trilep.");
        } else if (channel.find("Dilep") != string::npos || channel.find("Trilep") != string::npos){
            ATLASLabel(0.25,0.87,"Preliminary");
            myText(0.275,0.825,1,lumi.c_str(),0.042);
            myText(0.65,0.75,1,vlquark.c_str());
            myText(0.58,0.57,1,channel.c_str());
        }
        else {
            // OWN STYLE
        }
    
        //  Draw labels for contour [DRAWLABELS]
        //  it will use the file supplied by command line. The labels file is a plain text file with the following format for each line
        //  eo x y angle label
        //  eo -> an int which is 0 o expected and 1 for observed depending on which version of the plot the label will be written on.
        //  x -> x position of the label in user coordinates (not NDC).
        //  y -> y position of the label in user coordinates (not NDC).
        //  angle -> The angle used to write the label to fit the contour line
        //  label -> The actual text to write
        drawLabels(exob,labels);

        c->Update();
        string file = (exob == 0) ? "temperature_expected.pdf" : "temperature_observed.pdf";
        c->Print(file.c_str());
        delete c;
    }
    delete exp2D;
    delete obs2D;
    return 0;
}
