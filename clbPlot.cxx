#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "TH2D.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TPaveText.h"
#include "TF1.h"
#include "2DTools.h"
#include "TColor.h"
#include "TROOT.h"
#include "AtlasLabels.h"
#include "AtlasUtils.h"
#include "AtlasStyle.h"
#ifndef __CINT__
#include "AtlasLabels.C"
#include "AtlasUtils.C"
#include "AtlasStyle.C"
#endif


int clbPlot(string inputfile, string quark, string initmarker,string model)
{
    #ifdef __CINT__
    gROOT->LoadMacro("AtlasLabels.C");
    gROOT->LoadMacro("AtlasUtils.C");
    gROOT->LoadMacro("AtlasStyle.C");
    #endif

    // Define ATLAS style. 
    SetAtlasStyle();


    // Create the output parser
    OutputReader reader(inputfile,quark);
    reader.setInitMarker(initmarker);
    // List of values to read
    // --- THIS MIGHT BE MODIFIED FOR EACH USER DEPENDING ON WHAT
    // --- THEY HAVE READ FROM THE OUTPUT FILE.
    vector<string> lines;
    lines.push_back("gclsexpbm2");
    lines.push_back("gclsexpbm1");
    lines.push_back("gclsexpbmed");
    lines.push_back("gclsexpbp1");
    lines.push_back("gclsexpbp2");
    lines.push_back("1-CLb median  (sig)");
    lines.push_back("Observed CLs");
    lines.push_back("Observed CLb");
    reader.setLinesToFind(lines);
    // Parser file
    reader.parse();

    /// This is what will be searched for in the output file.
    // It can change depending on what the user wants.
    string keyToSearch = "Observed CLb";

    // Masses
    const int nmass = 11;
    int masses[nmass] = {350,400,450,500,550,600,650,700,750,800,850};

    // Loop over all masses
    vector<double> xMass;
    vector<double> yomCLB;
    for (int mi = 0; mi < nmass; mi++) {
        xMass.push_back((double)masses[mi]);
        vector<double> sfs,values;
        reader.getValue(sfs,values,masses[mi],"Observed CLb");
        size_t pos = std::find(values.begin(),values.end(),1)-values.end();
        if(pos >= values.size()){
            cerr<<"ERROR: There is not signal SF = 1 in the file "<<inputfile<<" for mQ = "<<masses[mi]<<endl;
            cerr<<"Please make sure you have these value in your log since this is the one used to get the CLB distribution"<<endl;
            return 1;
        }
        yomCLB.push_back(1-values[pos]);
    }

    // Create Tgraph to be used
    TGraph *graph = new TGraph(xMass.size(),&xMass[0], &yomCLB[0]);
    TCanvas *c= new TCanvas("c","c",800,600);
    c->SetRightMargin(0.05);

    string xtitle, ytitle,ztitle;
    ytitle = "1-CLb";
    xtitle = string("m_{")+quark+"} [ GeV]";
    graph->SetTitle((string(";")+xtitle+";"+ytitle).c_str());
    graph->SetMinimum(0.01);
    graph->SetMaximum(1.5);

    // Make the plot
    graph->SetLineColor(kAzure+3);
    graph->SetLineWidth(3);
    graph->SetMarkerStyle(20);
    graph->SetMarkerColor(kAzure+3);
    graph->Draw("CA");

    // Write text
    ATLASLabel(0.23,0.83,"Internal");
    string energy = "#sqrt{s} = 8 TeV";
    string lumi = "#int L dt = 20.3 fb^{-1}";
    string vlquark = string("#splitline{Vector-like ")+quark+"}{"+model+" model}";
    myText(0.7,0.73,1,energy.c_str());
    myText(0.62,0.83,1,lumi.c_str());
    myText(0.23,0.73,1,vlquark.c_str());
    c->Update();
    c->Print((string("omCLB_")+quark+"_"+model+".pdf").c_str());
    delete c;
    return 0;
}
