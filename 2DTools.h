/**
  The OutputReader class parse a txt file obtained from the outputs of McLimits
  for VLQ analysis and return the corresponding values asked given a key.
  */

#include <string>
#include <fstream>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>

vector<string> splitString(string s, const char *sep = ";"){
    //cout<<"Splitting string: "<<s<<endl;
    vector<string> result;
    string buf = "";
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == *sep){
            //cout<<"Before trim: "<<buf<<"----"<<endl;
            size_t fw = buf.find_first_not_of(" ");
            size_t lw = buf.find_last_not_of(" ");
            if (fw == std::string::npos) fw = 0;
            string trimmed = buf.substr(fw,lw-fw+1);
            //cout<<"After trim: "<<trimmed<<"----"<<endl;
            result.push_back(trimmed);
            buf="";
        } else {
            buf += s[i];
        }
    }
    //cout<<"Before trim: "<<buf<<"----"<<endl;
    size_t fw = buf.find_first_not_of(" ");
    size_t lw = buf.find_last_not_of(" ");
    if (fw == std::string::npos) fw = 0;
    string trimmed = buf.substr(fw,lw-fw+1);
    //cout<<"After trim: "<<trimmed<<"----"<<endl;
    result.push_back(trimmed);
    return result;
}

// Class to read the inputs from files.
// The idea is to receive a set of files. Read them and be able
// provide a set of values as needed.
class OutputReader
{
    public:
        OutputReader(std::string file, std::string quark) : mQuark(quark), mFile(file), mPreffix(""), mVerb(false){
        };
        ~OutputReader(){
        };

        // Parse file
        int parse(){
            if(mToFind.size() == 0){ 
                std::cerr<<"Warning: No keys to find have been suplied. No parsing will be done."<<std::endl; return 1;
            }
            std::vector<std::string> lines;
            // Loop over file and store all lines
            std::ifstream file(mFile.c_str());
            std::string lll;
            while(getline(file,lll)){
                //std::cout<<"Line: "<<lll<<std::endl;
                if(lll!="") lines.push_back(lll);
            }
            file.close();

            //std::cout<<"File read with "<<lines.size()<<" lines"<<std::endl;
            // Loop over all lines ad identify any problem.
            // If not, parse them
            size_t bsize = mToFind.size()+1;
            for (unsigned int l = 0; l < lines.size(); l+=bsize)
            {
                //std::cout<<"Linea "<<l<<std::endl;
                if(lines[l].find(mInitMarker) == std::string::npos){//TODO: This will be removed once tested
                    if (mVerb){std::cerr<<"Warning! The init marker \""<<mInitMarker<<"\" is not present in the init line "<<lines[l]<<std::endl;
                    std::cerr<<"Check that everything is fine."<<std::endl;
                    }
                    continue;
                }
                /*if(l+bsize < lines.size() && lines[l+bsize].find(mInitMarker) == std::string::npos) {
                  std::cerr<<"Warning! The file corresponding to "<<lines[l]<<" has not been filled properly."<<std::endl;
                  std::cerr<<"Maybe it needs to be resubmitted."<<std::endl;
                  return 1;
                  }*/
                if (mVerb) std::cout<<"Reading line: "<<lines[l]<<std::endl;
                // Once checked that everything is fine start filling data.
                data tmp;
                size_t prlength = mPreffix.size();
                std::string last = lines[l].substr(lines[l].rfind("/")+1+prlength);
                std::string mass = last.substr(0,last.find("_"));
                size_t initpos = last.find(mInitMarker);
                size_t brpos = last.find("_")+1;
                std::string BR = last.substr(brpos,initpos-1-brpos);
                if (mVerb) std::cout<<"BR: "<<BR<<", mass: "<<mass<<std::endl;

                tmp.init = lines[l];
                tmp.BRs = BR;
                tmp.mass = atoi(mass.c_str()); 
                //std::cout<<lines[l]<<std::endl<<BR<<", "<<tmp.mass<<std::endl;

                // Find values
                tmp.values.resize(mToFind.size());
                unsigned int found = 0;
                for (unsigned int i = 1; i <= bsize-1; ++i)
                {
                    std::string vline = lines[l+i];
                    vector<string> splits = splitString(vline,":");
                    std::string key = splits[0];
                    std::string value = splits[1];
                    if (mVerb) std::cout<<"Key: "<<key<<", value"<<value<<std::endl;
                    size_t pos = std::find(mToFind.begin(), mToFind.end(),key) - mToFind.begin();
                    if (pos > mToFind.size()){
                        if (mVerb) std::cerr<<"Warning! The key "<<key<<" has been found in file but you have not asked to find it"<<std::endl;
                    } else {
                        std::istringstream dvalue(value);
                        double converted;
                        dvalue >> converted;
                        tmp.values[pos] = converted;
                        found ++;
                    }
                }
                //std::cout<<"pasa loop"<<std::endl;
                if (found != mToFind.size()){
                    std::cerr<<"Error: only "<<found<<" items have been found but you specified "<<mToFind.size()<<std::endl;
                    std::cerr<<"Check that everything is fine for the line "<<lines[l]<<std::endl;
                    return 1;
                }
                //std::cout<<"Size: "<<mFileData.size()<<std::endl;
                mFileData.push_back(tmp);
                //std::cout<<"Push back"<<std::endl;
                //std::cout<<"Next line "<<lines[l+bsize]<<std::endl;
            }
            return 0;
        };

        void setInitMarker(std::string init){mInitMarker = init;};
        void setPreffix(std::string pr){mPreffix = pr;};
        void setLinesToFind(std::vector<std::string> lines){mToFind = lines;};
        void setVerbose(bool verb){mVerb = verb;};
        // Get the value depending on the br and mass of a given key
        double getValue(double wbr, double hbr, double zbr, int mass, std::string key){
            std::ostringstream ss; ss.precision(2);
            std::string w,h,z;
            if(mQuark == "T"){
                w="Wb"; z = "Zt"; h = "Ht";
            }
            if (mQuark == "B"){
                w="Wt"; z = "Zb"; h = "Hb";
            }
            ss << w;
            if(wbr == 0) ss << "0.0";
            else if (wbr == 1) ss << "1.0";
            else ss << wbr;
            ss << "_" << h;
            if(hbr == 0) ss << "0.0";
            else if (hbr == 1) ss << "1.0";
            else ss << hbr;
            ss << "_" << z;
            if(zbr == 0) ss << "0.0";
            else if (zbr == 1) ss << "1.0";
            else ss << zbr;
            //std::cout<<"Asking for "<<ss.str()<<", mass "<<mass<<std::endl;
            bool found = false;
            for (unsigned int i = 0; i < mFileData.size(); ++i)
            {
                data tmp = mFileData[i];
                /*if(mass == 800){
                  std::cout<<"BR: "<<tmp.BRs<<std::endl;
                  std::cout<<"ss: "<<ss.str()<<endl;
                  std::cout<<"File: "<<mFile<<endl;
                  }*/
                if(tmp.BRs == ss.str() && tmp.mass == mass){
                    found = true;
                    size_t pos = std::find(mToFind.begin(), mToFind.end(),key) - mToFind.begin();
                    if(pos >= tmp.values.size()){
                        if(mVerb){std::cerr<<"ERROR: You have requested: "<<key<<" which hasn't been found in the data structure."<<std::endl;
                        std::cerr<<" These elements hare stored: "<<tmp.values[0];
                        }
                        for (unsigned int v = 1; v < tmp.values.size(); ++v)
                        {
                            std::cerr<<", "<<tmp.values[v];
                        }				
                        std::cerr<<std::endl;
                        return -999.;
                    }
                    return (double)tmp.values[pos];
                }
            }
            if (!found) cerr<<"ERROR: You have requested the string "<<ss.str()<<" which has not been found in the file "<<mFile<<" for the mass "<<mass<<endl;
            return -999.;
        };
        void getValue(std::vector<double> &SF, std::vector<double> &values, int mass, std::string key){
            //std::cout<<"Asking for "<<ss.str()<<", mass "<<mass<<std::endl;
            for (unsigned int i = 0; i < mFileData.size(); ++i)
            {
                data tmp = mFileData[i];
                if(tmp.mass == mass){
                    /// Get the SF
                    size_t sfinit = tmp.init.find("_SF");
                    size_t sfend = tmp.init.find("_",sfinit+1);
                    std::string sSF = tmp.init.substr(sfinit+3,sfend-sfinit-3);
                    size_t pos = std::find(mToFind.begin(), mToFind.end(),key) - mToFind.begin();
                    if(pos >= tmp.values.size()){
                        std::cerr<<"ERROR: You have requested: "<<key<<" which hasn't been found in the data structure."<<std::endl;
                        std::cerr<<" These elements are stored: "<<tmp.values[0];
                        for (unsigned int v = 1; v < mToFind.size(); ++v)
                        {
                            std::cerr<<", "<<mToFind[v];
                        }				
                        std::cerr<<std::endl;
                    } else{
                        SF.push_back(atof(sSF.c_str()));
                        values.push_back(tmp.values[pos]);
                    }
                }
            }
        };
        std::string getFileName(){return mFile;};

    private:
        std::string mInitMarker;
        std::string mQuark;
        std::vector<std::string> mToFind;
        std::string mFile;
        std::string mPreffix;
        struct data{
            std::string init;
            std::string BRs;
            int mass;
            std::vector<double> values;
        };
        std::vector<data> mFileData;
        bool mVerb;

};
