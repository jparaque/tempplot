#!/bin/bash


version="Summary_comb_global"
mkdir -p PlotsTT_$version
mkdir -p PlotsBB_$version
# Do 2D temperature plots for BB and TT
root -q -b -l "TempPlot.cxx+(\"MergedOutputs_SS.txt\",\"B\",\"BBS_JO_master_DEFAULT_SF1_h_nominal\",\"Summary1\",\"labels_B_1.txt\")"
mv *{pdf,png,eps} PlotsBB_$version/
mv expected*txt PlotsBB_$version/
mv observed*txt PlotsBB_$version/
root -q -b -l "TempPlot.cxx+(\"MergedOutputs_SS.txt;MergedOutputs_Ztag.txt\",\"B\",\"BBS_JO_master_DEFAULT_SF1_h_nominal;bb_2d_comb_RebinZbm_SF1_cut7_ZbmaxPt_invMass\",\"Summary2\",\"labels_B_2.txt\")"
mv *{pdf,png,eps} PlotsBB_$version/
mv expected*txt PlotsBB_$version/
mv observed*txt PlotsBB_$version/
root -q -b -l "TempPlot.cxx+(\"MergedOutputs_SS.txt;MergedOutputs_Ztag.txt;Output_WtX.txt\",\"B\",\"BBS_JO_master_DEFAULT_SF1_h_nominal;bb_2d_comb_RebinZbm_SF1_cut7_ZbmaxPt_invMass;WtX\",\"Summary3\",\"labels_B_3.txt\")"
mv *{pdf,png,eps} PlotsBB_$version/
mv expected*txt PlotsBB_$version/
mv observed*txt PlotsBB_$version/
root -q -b -l "TempPlot.cxx+(\"MergedOutputs_SS.txt;MergedOutputs_Ztag.txt;Output_WtX.txt;MergedGrid_HbX_roostat.txt\",\"B\",\"BBS_JO_master_DEFAULT_SF1_h_nominal;bb_2d_comb_RebinZbm_SF1_cut7_ZbmaxPt_invMass;WtX;combinationAllSyst_p0\",\"Summary4\",\"Labels_B_4.txt\")"
mv *{pdf,png,eps} PlotsBB_$version/
mv expected*txt PlotsBB_$version/
mv observed*txt PlotsBB_$version/





root -q -b -l "TempPlot.cxx+(\"MergedOutputs_SS.txt\",\"T\",\"TTS_JO_master_DEFAULT_SF1_h_nominal\",\"Summary1\",\"labels_T_1.txt\")"
mv *{pdf,png,eps} PlotsTT_$version/
mv expected*txt PlotsTT_$version/
mv observed*txt PlotsTT_$version/
root -q -b -l "TempPlot.cxx+(\"MergedOutputs_SS.txt;MergedOutputs_Ztag.txt\",\"T\",\"TTS_JO_master_DEFAULT_SF1_h_nominal;tt_2d_comb_RebinZbm_SF1_cut7_ZbmaxPt_invMass\",\"Summary2\",\"labels_T_2.txt\")"
mv *{pdf,png,eps} PlotsTT_$version/
mv expected*txt PlotsTT_$version/
mv observed*txt PlotsTT_$version/
root -q -b -l "TempPlot.cxx+(\"MergedOutputs_SS.txt;MergedOutputs_Ztag.txt;MergedGrid_WbHt_roostat.txt\",\"T\",\"TTS_JO_master_DEFAULT_SF1_h_nominal;tt_2d_comb_RebinZbm_SF1_cut7_ZbmaxPt_invMass;combinationAllSyst_p0\",\"Summary3\",\"Labels_T_3.txt\")"
mv *{pdf,png,eps} PlotsTT_$version/
mv expected*txt PlotsTT_$version/
mv observed*txt PlotsTT_$version/
